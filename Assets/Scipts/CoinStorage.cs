using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CoinStorage : MonoBehaviour
{
    [SerializeField] private int maxAmount;
    [SerializeField] private int curAmount;

    [SerializeField] private Text amountText;
    [SerializeField] private Image progressBar;

    [SerializeField] private Text addedAmountText;
    [SerializeField] private float addFeedbackTime = 0.2f;

    private void Start()
    {
        UpdateUIElements();
    }

    public void IncreaseAmount(int amount)
    {
        curAmount = Mathf.Min(maxAmount, curAmount + amount);
        StartCoroutine(ShowAddedAmountText(amount));
        UpdateUIElements();
    }

    public bool TakeAmount(int amount)
    {
        if (curAmount < amount)
            return false;
        curAmount -= amount;
        UpdateUIElements();
        return true;
    }

    private void UpdateUIElements()
    {
        amountText.text = curAmount.ToString() + '/' + maxAmount;
        float fillAmount = curAmount / (float) maxAmount;
        if(curAmount > 50)
            fillAmount = Mathf.Max(fillAmount, 0.17f);
        progressBar.fillAmount = fillAmount;
    }

    private void OnValidate()
    {
        UpdateUIElements();
    }

    private IEnumerator ShowAddedAmountText(int addedAmount)
    {
        addedAmountText.text = addedAmount.ToString() + '+';
        addedAmountText.gameObject.SetActive(true);
        float timePassed = 0;
        while (timePassed < addFeedbackTime)
        {
            timePassed += Time.deltaTime;
            yield return null;
        }
        addedAmountText.gameObject.SetActive(false);
    }
    
}
