using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinGenerator : MonoBehaviour
{
    [Header("Dependencies")]
    [SerializeField] private CoinStorage storage;
    
    [Header("Upgrade Formula")]
    [SerializeField] private int baseGeneratedAmount;
    [SerializeField] private int baseGeneratedAmountCoeff;
    [SerializeField] private int baseUpgradeCost;
    [SerializeField] private int baseUpgradeCostCoeff;
    
    [Header("Level")]
    [SerializeField] private int level = 0;

    [Header("Unlocked Objects")] 
    [SerializeField] private List<GameObject> unlockableObjects;
    [SerializeField] private List<int> unlockLevels;

    [Header("Flags")] 
    [SerializeField] private bool isPaused;
    
    [Header("UI Elements")] 
    [SerializeField] private Text levelText;
    [SerializeField] private Text upgradeCostText;


    private float timePassed;

    private void Start()
    {
        UpdateUIElements(level);
    }

    private void Update()
    {
        HandleGeneration();
    }

    private void HandleGeneration()
    {
        if(isPaused)
            return;
        timePassed += Time.deltaTime;
        if (timePassed >= 1f)
        {
            timePassed = 0f;
            Generate();
        }
    }
    
    public void Generate()
    {
        int generatedAmount = CalculateGeneratedAmount();
        storage.IncreaseAmount(generatedAmount);
    }
    
    public void LevelUp()
    {
        int upgradeCost = CalculateUpgradeCost();
        if (storage.TakeAmount(upgradeCost))
        {
            level++;
            UpdateUIElements(level);
            UnlockMaterial();

        }
        Debug.Log("Failed");
    }

    private void UpdateUIElements(int level)
    {
        levelText.text = "Level: " + level;
        upgradeCostText.text = CalculateUpgradeCost().ToString();
    }


    private void UnlockMaterial()
    {
        for (int i = 0; i < unlockLevels.Count; i++)
            if (unlockLevels[i] <= level)
                if(i < unlockableObjects.Count)
                    unlockableObjects[i].SetActive(true);
    }

    private int CalculateGeneratedAmount()
    {
        return baseGeneratedAmount + baseGeneratedAmountCoeff * level;
    }

    private int CalculateUpgradeCost()
    {
        return baseUpgradeCost + baseUpgradeCostCoeff * level;
    }
    
    public void GetDailyRewards(int amount)
    {
        storage.IncreaseAmount(amount);
    }

    public void ChangeActiveState(bool isPaused)
    {
        this.isPaused = isPaused;
    }
}
